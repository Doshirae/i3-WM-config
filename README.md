# My i3 configuration (and more)

## Fast Install for Arch user :D


`git https://gitlab.com/feliwyn/i3-WM-config.git && mv i3-WM-config ~/.i3-WM-config && cd ~/.i3-WM-config && ./install.sh`


## Requirement
* ArchLinux user, just type commande below.
* For other user, you need thoses package :
 * i3-gaps
 * conky
 * compton
 * feh
 * rofi
 * meslo-font
 * gtk-arc-theme
 * moka-icon-theme
 * awesome-font
 * terminator
 * claws-mail
 * pcmanfm
 * volumeicon
 * profile-sync-daemon (or change config `exec psd` in `exec chromium` or whatever)
 * chromium
 
* All other are optional

## My Commands

**mod = Super**

* mod+arrow = change container
* alt+shift+arrow = moove container
* mod+v = vertical container
* mod+h = horizontal container

* mod+enter = Terminator in Workspace 6
* mod+Shift+enter = Terminator in current Workspace

* mod+w = Web Browser
* mod+e = File Manager
* mod+m = Mail
