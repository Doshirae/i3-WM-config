(when (>= emacs-major-version 24)
  (require 'package)
  (add-to-list
   'package-archives
   '("melpa" . "http://melpa.org/packages/")
   t)
  (package-initialize))
(custom-set-variables
 '(custom-enabled-themes (quote (wombat)))
 '(inhibit-default-init nil)
 '(inhibit-startup-screen nil)
)

;;disable backup file 
(setq make-backup-files nil)

;;(setq auto-save-file-name-transforms           `((".*" ,temporary-file-directory t))) 


(require 'multiple-cursors)

(global-set-key (kbd "C-z") 'mc/edit-lines)
(global-set-key (kbd "C-Z") 'mc/mark-pop)

(global-set-key (kbd "C->") 'mc/mark-next-like-this)
(global-set-key (kbd "C-<") 'mc/mark-previous-like-this)
(global-set-key (kbd "C-c C-<") 'mc/mark-all-like-this)


;; hook
(add-hook 'markdown-mode-hook 'pandoc-mode)
